SUBROUTINE AGRAPH(X, Y, N)

    !
    !         ***************************************
    !         PLOTTING PACKAGE TO PLOT AN X-Y GRAPH WITH N POINTS
    !         AUTHOR- JOHN B. MANDER
    !         DATE/VERSION 1983/1
    !         ***************************************
    !
    COMMON/CARD/NSTOP, NTRAP, INPUT(80)
    COMMON/KARD/RRDR(10), IRDR(10)
    !
    INTEGER N
    !
    REAL X(N), Y(N), XNAME(14), YNAME(14), GNAME(14)
    REAL TYPE1(4), FMT(1)
    !
    DATA TYPES/20*H0, 3*MM ,INK, PEN, PLEASE/
    DATA FMT(1)/6,H(F6,.1)/
    !
    ! READ PLOTTING DATA CARDS (FOUR CARDS)
    !
    CALL READR(6, 0, 0, RRDR, IRDR)
    WRITE(6, 616) INPUT
    XSCALE = RRDR(1)
    YSCALE = RRDR(2)
    XINC = RRDR(3)
    YINC = RRDR(4)
    X0 = RRDR(5)
    YO = RRDR(6)
    !
    READ(5, 555) GNAME
    READ(5, 555) XNAME
    READ(5, 555) YNAME
    WRITE(6, 626) GNAME
    WRITE(6, 636) XNAME
    WRITE(6, 646) YNAME
    !
    IF(N==1) GO TO 999
    IF(XSCALE==0.0.OR.YSCALE==0.0)
        !
        !     DETERMINE MAXIMUM COORDINATES
        !
        DO K = 1, N
            IF(X(K)<XMIN) XMIN = X(K)
            IF(X(K)>XMAX) XMAX = X(K)
            IF(Y(K)<YMIN) YMIN = Y(K)
            IF(Y(K)>YMAX) YMAX = Y(K)
        end do
        !
        !     MESH SIZE DETERMINATION
        !
        NXN = 0.99 - XMIN / XINC
        NYN = 0.99 - YMIN / YINC
        NXP = 0.99 + XMAX / XINC
        NYP = 0 99 + YMAX / YINC
        IXINC = 100. * XINC / XSCALE + 0.5
        IYINC = 100. * YINC / YSCALE + 0.5
        LENGTH = IXINC * FLOAT(NXP + NXN + 1) + 600
        IX = -NXN * IXINC
        IY = -NYN * IYINC
        !
        ! PLOT LINE
        CALL AINIT(LENGTH)
        CALL ASPEED(5)
        CALL ATYPE(TYPE1, 24)
        CALL AORIG(-1 * IX + 400, -1 * IY + 100)
        CALL ALINE(X, Y, N, 0.0, 0.0, XSCALE, YSCALE)
        !
        !PLOT BOXES
        !
        CALL ABOX(00, -1, NXP + 1, 01, IXINC, 1, 1)
        CALL ABOX(-1, 00, 01, NYP + 1, 1, IYINC, 1)
        CALL ABOX(IX, -1, NXN, 01,IXINC, 1, 1)
        CALL ABOX(-1,IY, 01, NYN, 1, IYINC,1)
        !
        !     WRITE THE X AND Y SCALE NUMBERS
        !
        IF(XO>0.0) then
            CALL ASCALE&
                (1 * IXINC - 45, -20, 1 * IXINC, O, 1 * X0, 1 * X0, NXP, 1, 2, FMT, 6)
        end if
        IF(YO>0 0) then
            CALL ASCALE&
                (-70, 1 * IYINC - 05, 0, 1 * IYINC, 1 * Y0, 1 * Y0, NYP, 1, 2, FMT, 6)
        end if
        IF (X0>0.0) then
            CALL ASCALE&
            (-1*IXINC-45, -20,-1*IXINC, 0, -1*X0, -1*X0, NXN, 1, 2, FMT, 6)
        end if
            IF(Y0>0.0) then
            CALL ASCALE&
                    (70, -1 * IYINC - 05, 0, -1 * IYINC, -1 * Y0, -1 * YO, NYN, 1, 2, FMT, 6)
        end if
            !
            !     LABEL X AND Y AXES, THEN WRITE GRAPH TITLE
            !
            CALL ALAB(10, -40, XNAME, 80.1, 2)
            CALL ALAB(-75, 10, YNAME, 80, 1.4)
            CALL ALAB(IX, IY - 60, GNAME, 80, 1, 2)
            !
            CALL AEND
            !
            999 RETURN
            555 FORMAT(13A6, A2)
            616 FORMAT(40H0 PLOT VALUES XSCALE, YScALE, XINC, YINC, /2H (80A1, 1H))
            626 FORMAT(23H0 GRAPH CAPTION, /2H (13A6, A2, 1H))
            636 FORMAT(32H0 LABEL TO BE GIVEN TO X AXIS, /2H 1/13A6, A2, 1H)
            646 FORMAT(33H0 LABEL TO BE GIVEN TO Y AXIS, /2H (13A6, A2, 1H))

    END IF
end subroutine AGRAPH