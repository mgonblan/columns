SUBROUTINE REBAR(N, NS, E, FS, YSTEEL, FLAST, ELAST, EOLD, &
        FY, FSU, ESH, ESU, YOUNGS, YTAN, ES, EO, FO, EB, FB, &
        EOO, FOO, EMAX, FMAX, EMO, YOUNGQ, FC, R, Q)

    !
    !     ***********************************************************
    !      CYCLIC LOADING STRESS-STRAIN MODEL FOR REINFORCING STEEL
    !      AUTHOR = JOHN B.MANDER
    !      DATE/VERSION 1983/01
    !
    !      VARIABLES PASSED TO SUBROUTINE. COMPRESSION M=1, TENSION M=2
    !      ==============================
    !       N,
    !      NS,
    !     E,
    !     FS,
    !     YSTEEL,
    !     FLAST,
    !     ELAST,
    !     EOLD,
    !     FY,FSU,ESH,ESU,YOUNGS,YTAN,ES,EO,FO,EB,FB,
    !     EOO,FOO,EMAX,FMAX,EMO,YOUNGQ,FC,R,Q
    INTEGER M, N, NS
    REAL    E(NS), FS(NS), YSTELL(NS), FLAST(NS), ELAST(NS), EOLD(NS)
    REAL    FY(2), FSU(2), ESH(2), ESU(2), YOUNGS(2), YTAN(2)
    REAL    ES(2, NS), EO(2, NS), FO(2, NS), EB(2, NS), FB(2, NS)
    REAL    YOUNGQ(2, NS), FC(2, NS), R(2, NS), Q(2, NS)
    !
    !     STAGING DIRECTION, LOADING (M,K,S)=(2,1,1.) UNLOADING=(1,2,-1.)
    !
    DELTA = E(N) - ELAST(N)
    M = 2
    K = 1
    S = 1.
    IF(DELTA<0.0) M = 1
    IF(DELTA<0.0) K = 2
    IF(DELTA<0.0) S = -1.
    IF(DELTA==0.0) RETURN
    P = YTAN(M) * (ESU(M) - ESH(M)) / (FSU(M) - FY(M))
    IF(ES(1, N)==0.0.AND.ES(2, N)==0.0) GO TO 110
    IF(((ELAST(N) - EOLD(N)) * DELTA<0.0)) GO TO 200
    !
    !      GOVERNING STRESS-STRAIN CURVES
    !      ==============================
    !
    20 IF(((E(N) - EOO(M, N) * S)>0.0.AND.&
            ((E(N) - EO(K, N)) * S<0.0))) GO TO 135
        IF(((E(N) - EOO(M, N) * S)<0.0)) GO TO 130
        110 RATIO = (ESU(M + EMO(M, N) - E(N)) / (ESU(M) - ESH(M)))
        IF(RATIO<1) GO TO 114
        IF(((E(N) - EMO(M, N) - FY(M) / YOUNGS(K)) * S)>0.0) GO TO 112
        !
        !     ELASTIC BRANCH
        !
        YS(N) = (E(N) - EMO(M, N)) * YOUNGS(M)
        YSTEEL(N) = YOUNGS(M)
        GO TO 444
        !
        !     YIELD PLATEAU
        !
        112 ES(M, N) = E(N) - EMO(M, N)
        FS(N) = FY(M)
        YSTEEL(N) = 0.
        GO TO 444
        !
        !     WORK HARDENED SKELETON
        !
        114 SIGN = 1.0
        IF(RATIO<0.) SIGN = -1.0
        ES(M, N) = E(N) - EMD(M, N)
        PP = 1.0 - 1.0 / P
        FS(N) = FSU(M) - (FSU(M) - FY(M)) * (ABS(RATIO))**P
        YSTEEL(N) = YTAN(M) * SIGN * ((FSU(M) - FS(N)) / (FSU(M) - FY(M)))**PP
        GO TO 444
        !
        !     SOFTENED BRANCH
        !
        130 CANT = ((E(N) - EO(M, N)) * YOUNGQ(M, N) / (FC(M, N) - FO(M, N)))**R(M, N)
        RR = 1.0 / R(M, N)
        SECANT = YOUNGQ(M, N) * (Q(M, N) + (1. - Q(M, N)) / (1. + CANT)**RR)
        FS(N) = SECANT * (E(N) - EO(M, N)) + FO(M, N)
        YSTEEL(N) = SECANT - (SECANT - Q(M, N) * YOUNGQ(M, N)) * CANT / (1.0 + CANT)
        GO TO 444
        !
        !     ELASTIC LOADING FOR REVERSALS LESS THAN YIELD STRAIN
        !
        135 FS(N) = YOUNGS(M) * (E(N) - ECO(M, N)) + FOO(M, N)
        YSTELL(N) = YOUNGS(M)
        GO TO 444
        !
        !     ==============================================================
        !     CHANGE IN STRAINING DIRECTION FROM REVERSAL COORDINATE (E0,F0)
        !     EVALUATION OF REVERSAL PARAMETERS EB,FB,Q,R,YOUNGQ
        !     ==============================================================
        !
        200 YIELD = FY(2)
        IF(((F0(K, N) + FLAST(N)) / 2.0)<0.) YIELD = FY(1)
        DELF = ABS((F0(K, N) - FLAST(N)) / YIELD)
        IF(DELF<1.) FOO(M, N) = FLAST(N)
        IF(DELF<1.) EOO(M, N) = ELAST(N)
        IF(DELF<1.) GO TO 135
        E00(M, N) = EO(K, N)
        EO(M, N) = ELAST(N)
        IF((F0(M, N) / FMAX(K, N))>=0.99999) EMAX(K, N) = EO(M, N)
        IF((F0(M, N) / FMAX(K, N))>=0.99999) FMAX(K, N) = FO(M, N)
        !
        FB(M, N) = FMAX(M, N)
        FRATIO = ABS((FSU(K) - FMAX(K, N)) / (FSU(K) - FY(K)))
        IF(FRATIO<1.) PINV = (FSU(K) - FY(K)) / (ESU(K) - ESH(K)) / YTAN(K)
        IF(FRATIO<1.) ES(K, N) = ESU(K) - (ESU(K) - ESH(K)) * FRATIO**PINV
        IF(((FO(M, N) - FB(K, N)) * S)>(FY(2) / 10000.)) GO TO 230
        !
        !     SKELETON BRANCH REVERSAL, FIND NEW JUNCTION COORDINATE (EB,FB)
        !
        ERATIO = ABS((ESU(M) + ES(K, N)) / (ESU(M) - ESH(M)))
        IF(ERATIO<1.) FB(M, N) = FSU(M) - (FSU(M) - FY(M)) * ERATIO**P
        FRATIO = (FSU(M) - FB(M, N)) / (FSU(M) - FY(M))
        ESHIFT = (ES(K, N)**2 / ESU(2) + 2.0 * (ABS(FY(M) / YOUNGS(M)))) * S
        IF(ERATIO>1.0.AND.(ABS(FO(M, N) / FY(K)))<1.001) ESHIFT = 0.0
        EMO(M, N) = EO(M, N) - FO(M, N) / YOUNGS(K) + ESHIFT
        EB(M, N) = EMO(M, N) - ES(K, N)
        GO TO 250
        !
        !     SOFTENED BRACH REVERSAL, FIND NEW JUNCTION COORDINATE (EB,TB)
        !
        230 EXTRA = ESU(2)*(EO(M,N)-EB(K,N))-0.5*FY(M)/YOUNGS(M)
            IF((EMAX(M,N)/ESH(M))<1.0) EXTRA = ESU(2)*(E0(M,N)-EB(K,N))
            IF(DELF>1.1) EB(M,N) = EMAX(M,N)+EXTRA
            IF(((EB(MN)-E0(M,N)-(FB(M,N)-FO(M,N))/YOUN (K ))*S)<=0.0) THEN
                EB(M,N) = EMAX(M, N) +FY(M)/YOUNGS(K)/2.
            END IF

            FRATIO = (FEU (M)-FB(M,N) ) / (ESU(M)-FY(M))
            IF(FRATIO<1.) THEN
               EMO(M,N) = EB(M,N)-ESU(M)+(ESU(M)-ESH(M))*FRATIO**(1.0/P)
            END IF

            EMAX(K,N) = EB(K,N)
            FMAX(K,N) = FB(K,N)
!
!           DETERMINATION• OF Q, R, AND YOUNGQ BY ITERATION
!

        250 EJOIN = (FY(M) - YTAN(M) * ESH(M)) / (YOUNGS(M) - YTAN(M))
        YTANG = (FB(M, N) - EJOIN * YOUNGS(M)) / (EB(M, N) - EMO(M, N) - EJOIN)
        IF(FRATIO<1.) YTANG = YTAN(M) * FRACTIO**(1.-1/P)
        YSEC = (FB(M, N) - FO(M, N)) / (EB(M, N) - EO(M, N))
        YOUNGQ(M, N) = YOUNGS(K)
        QSLOPE = YTANG / 2.
        FC(M, N) = FB(M, N) - QSLOPE * &
                (YOUNGS(K) * (EB(M, N) - EO(M, N)) - FB(M, N) + FO(M, N)) / (YOUNGS(K) - YTANG)
        EDF = ABS(YOUNGQ(M, N)*(EB(M, N)-EO(M, N))/(FC(M, N)-FO(N, M)))
        260 R(M, N) = ALOG(ABS((YSEC - YTANG) / (YTANG - QSLOPE))) / ALOG(EDF)
        IF(R(M, N)<1.0) R(M, N) = 1.0
        IF(R(M, N)>20.) R(M, N) = 20.0
        FACTOR = (1.0 + EDF**R(M, N))**(1. / R(M, N))
        QSLOPE = (QSLOPE + (FACTOR * YSEC - YOUNGQ(M, N)) / (FACTOR - 1.)) / 2.
        YOUNGQ(M, N) = YOUNGS(K) / (&
                QSLOPE / YOUNGQ(M, N) + (1. - QSLOPE / YOUNGQ(M, N)) / &
                        (1. + ((FY(M) * YOUNGQ(M, N) / YOUNGS(K)) / (FC(M, N) - FO(M, N)))**R(M, N))&
                                **(1. / R(M, N)))
        IF((YOUNGQ(M, N) / YOUNGS(K))>1.50)&
                YOUNGQ(M, N) = YOUNGS(K) * 1.5
        Q(M, N) = QSLOPE / YOUNGQ(M, N)
        IF(R(M, N)>19.) GO TO 130
        EDF = YOUNGQ(M, N) * (EB(M, N) - EO(M, N)) / (FC(M, N) - FO(M, N))
        EDF = ABS(EDF)
        IF(R(M, N)==1.0.AND.KOUNT>3) GO TO 130
        YTEST = YSEC - (YSEC - QSLOPE) / (1. + 1. / (EDF**R(M, N)))
        ERROR = ABS(YTANG - YTEST) / YTANG
        KOUNT = KOUNT + 1
        IF(ERROR>0.005.AND.KOUNT<10) GO TO 260
        GO TO 130
        !
        444 CONTINUE
        IF(YSTELL(N)<=0.) YSTELL(N) = 20.
        IF(FS(N)<FSU(1)) YSTELL(N) = 1.0
        IF(FS(N)>FSU(2)) YSTELL(N) = 2.0
        IF(FS(N)<FSU(1)) FS(N) = FMAX(1, N)
        IF(FS(N)>FSU(2)) FS(N) = FMAX(2, N)
        RETURN
END
