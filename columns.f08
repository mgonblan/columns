program columns
    implicit none
    !
    ! ***********************
    ! program columns
    ! column analysis program
    ! author= JOHN B MANDER
    ! date / version = 1983/01
    ! ************************
    common/card/nstop ,ntrap ,input(80)
    common/kARD/RRDR(10) ,IRDR(10) ,TITLE(80)
    !
    real AC(77) ,XPHI(999) ,YMOM(999) ,XDFN(999) ,YFOR(999)
    real YSEC(77) ,YJSEC(77) ,XSEC(77) ,XSECOV(77)
    real WISEC(77) ,WJSEC(77) ,WICONC(77) ,WJCONC(77) ,WIS(22) ,WJS(22)
    real YP(25) ,XM(25) ,TNMM(99) ,PHIMM(99)
    !
    ! DIMENSIONING VARIABLES FOR SUBROUTINE PMPHI
    !
    integer MODE ,KTYPE ,NOTDYN ,NUM ,ITER ,J ,N ,NE ,NE
    real    ESIDE ,YSIDE ,PNAIM ,PHIAIM ,EQAIM
    real    YI(77) ,YJ(77) ,YC(77) ,ACOVER(77) ,ACORE(77) ,AS(22) ,YS(22)
    real    EDOTF ,EDOTY ,DF ,DY ,DINDEX ,TIME
    real    BETAI ,TOLERN ,TOLERM ,TOLPHI ,TOLREQ
    real    EI ,EA ,EZ ,S11 ,S12 ,S21 ,S22 ,PN ,TM ,YB ,PHI ,EQ ,CC
    !
    ! Common arguments for subroutine concrt
    !
    common/CON1/EC(77) ,FCONC(2 ,77) ,YCONC(2 ,77) ,FCLAST(2 ,77) ,ECLAST(2 ,77)
    common/CON2/ECOLD(77) ,FPC ,FPT ,YOUNGC ,YKONK ,ECO ,ESPALL ,ECTEN
    common/CON3/FLAT ,SRATIO ,ECU ,SK ,FPCC ,ECC
    common/CON4/FPEAK(2 ,77) ,EPEAK(2 ,77) ,FCTEN(2 ,77) ,ECULT(2 ,77)
    common/CON5/ECUN(77) ,FCUN(2 ,77) ,YOUNGU(2 ,77) ,ECPLAS(2 ,77)
    common/CON6/ECLO(77) ,FCLO(2 ,77) ,FCLO(2 ,77) ,FCNEW(2 ,77) ,ECRETN(2 ,77)
    common/CON7/FCRETN(2 ,77) ,YCRETN(2 ,77) ,ECMIN(77) ,ECMAX(77)
    !
    ! Common arguments for subroutine rebar
    !
    common/REBAR1/E(22) ,FS(22) ,YSTEEL(22) ,FLAST(22) ,ELAST(22) ,EOLD(22)
    common/REBAR2/FY(2) ,FSU(2) ,ESH(2) ,ESU(2) ,YOUNGS(2) ,YTAN(2)
    common/REBAR3/ES(2 ,22) ,EO(2 ,22) ,FO(22) ,EB(2 ,22) ,FB(2 ,22)
    common/REBAR4/EOO(2 ,22) ,FOO(2 ,22) ,EMAX(2 ,22) ,FMAX(2 ,22) ,EMO(2 ,22)
    common/REBAR5/YOUNGQ(2 ,22) ,FC(2 ,22) ,R(2 ,22) ,Q(2 ,22)
    !
    ! DATA CARD INPUT
    ! ===============
    !
    WRITE(6 ,600)
    !
    ! CARD 1 TITLE CARD
    !
    READ(5 ,501) TITLE
    WRITE(6 ,601) TITLE
    !
    ! CARD 2 UNCONFINED CONCRETE PROPERTIES
    CALL READR(5 ,0 ,0 ,RRDR ,IRDR)
    WRITE(6 ,602) INPUT
    FPC = -ABS(RRDR(1))
    FPT = ABS(RRDR(2))
    YOUNGC = ABS(RRDR(3))
    ECO = -ABS(RRDR(4))
    ESPALL = -ABS(RRDR(5))
    YKONK = YOUNGC
    EDOTF = 0.035 * FPC * FPC
    EDOTY = ABS(EDOTF * FPC)
    DINDEX = 1.0 / 6.0
    DF = 1. / (1. + (0.00001 / EDOTF)**DINDEX)
    DY = 1. / (1. + (0.00001 / EDOTY)**DINDEX)
    !
    ! CARD 3 CONFINED CONCRETE PROPERTIES
    !
    CALL READR(4 ,0 ,0 ,RRDR ,IRDR)
    WRITE(6 ,603) INPUT
    FPCC = -ABS(RRDR(1))
    ECC = -ABS(RRDR(2))
    ECU = -ABS(RRDR(3))
    SEHOOP = ABS(RRDR(4))
    ECTEN = FPT / YOUNGC
    SK = FPCC / FPC
    SRATIO = (ECC / ECO - 1.0) / (SK - 0.9999)
    IF(SRATIO<=1.0) SRATIO = 5.0
    !
    !   CARD 4 STEEL PROPERTIES-COMPRESSION
    !
    CALL READR(10 ,0 ,0 ,RRDR ,IRDR)
    WRITE(6 ,604) INPUT
    FY(1) = -ABS(RRDR(1))
    FSU(1) = -ABS(RRDR(2))
    ESH(1) = -ABS(RRDR(3))
    ESU(1) = -ABS(RRDR(4))
    YOUNGS(1) = ABS(RRDR(5))
    YTAN(1) = ANS(RRDR(6))
    do N = 1 ,22
        FMAX(1 ,N) = FY(1)
        FB(1 ,N) = FY(1)
    end do
    !
    !   CARD 5 STEEL PROPERTIES-TENSION
    !
    CALL READR(10 ,0 ,0 ,RRDR ,IRDR)
    WRITE(6 ,605) INPUT
    FY(2) = ABS(RRDR(1))
    FSU(2) = ABS(RRDR(2))
    ESH(2) = ABS(RRDR(3))
    ESU(2) = ABS(RRDR(4))
    YOUNGS(2) = ABS(RRDR(5))
    YTAN(2) = ABS(RRDR(6))
    DO N = 1 ,22
        DMAX(2 ,N) = FY(2)
        FB(2 ,N) = FY(2)
    END DO
    !
    !   CARD 6 CONTROL PARAMETERS
    !
    WRITE(6 ,606) INPUT
    NSEC = IRDR(1)
    NE = IRDR(2)
    NSTEEL = IRDR(3)
    !
    !   SECION PROPERTIES
    !
    NCARD = 7
    IF(NSEC>=1) THEN
        DO K = 1 ,NSEC
            CALL READR(6 ,1 ,0 ,RRDR ,IRDR)
            NCARD = 6 + IRDR(1)
            WRITE(6 ,6073) NCARD ,INPUT
            IF(K/=IRDR(1)) STOP
            YISEC(K) = RRDR(1)
            YJSEC(K) = RRDR(2)
            XSEC(K) = RRDR(3)
            XSECOV(K) = RRDR(4)
            WISEC(K) = ABS(RRDR(5))
            WJSEC(K) = ABS(RRDR(6))
            ASEC = (YJSEC(K) - YISEC(K)) * XSEC(K)
            AGROSS = AGROSS + ASEC
            YAG = YAG + ASEC * (YJSEC(K) + YISEC(K)) * 0.5
            YFORC = YAG / AGROSS
        END DO
    END IF
    !
    !   CARD 7 CIRCULAR SECTION
    !
    CALL READR(3 ,0 ,0 ,RRDR ,IRDR)
    WRITE(6 ,6071) INPUT
    DIAM = RRDR(1)
    COVER = RRDR(2)
    DIAMST = RRDR(3)
    AGROSS = 0.7854 * DIAM**2
    IF(NTYPE==1) NSTEEL = 1
    !
    !   CARD 8 STEEL POSITIONS
    !
    DO KS = 1 ,NSTEEL
        NCARD = NCARD + 1
        CALL READR(4 ,2 ,0 ,RRDR ,IRDR)
        WRITE(6 ,608) NCARD ,INPUT
        IF(KS/=IRDR(1)) STOP
        NBARS = IRDR(2)
        DBARS = RRDR(1)
        AS(KS) = 0.7854 * FLOAT(NBARS) * RRDR(1)**2
        YS(KS) = RRDR(2) - YFORC
        WIS(KS) = ABS(RRDR(3))
        WJS(KS) = ABS(RRDR(4))
        AST = AST + AS(KS)
    END DO
    !
    !   CARD9 SHEAR SECTION PROPERTIES
    !
    CALL READR(7 ,0 ,0 ,RRDR ,IRDR)
    NCARD = NCARD + 1
    WRITE(6 ,609) NCARD ,INPUT
    HEIGHT = ABS(RRDR(1))
    HINGEL = ABS(RRDR(2))
    SHACON = ABS(RRDR(3))
    FORMF = ABS(RRDR(4))
    SHSTNH = ABS(RRDR(5))
    SHSTPL = ABS(RRDR(6))
    YPTRAT = ABS(RRDR(7))
    !
    !   CARD 10 AXIAL LOAD ON COLUMN (TENSION IS +VE) P-DELTA EFFECTS
    !
    CALL READR(3 ,0 ,0 ,RRDR ,IRDR)
    NCARD = NCARD + 1
    WRITE(6 ,610) NCARD ,INPUT
    PF = RRDR(1)
    PDELTA = RRDR(2)
    VRATIO = RRDR(3)

    YTHICK = (YJSEC(NSEC) - YISEC(1)) / NE
    YI(1) = YISEC(1) - YFORC
    I = 0
    DO K = 1 ,NSEC
        N = (YJSEC(K) - YISEC(K)) / YTHICK
        DO NN = 1 ,N
            I = I + 1
            YJ(I) = YISEC(K) + NN * (YJSEC(K) - YISEC(K)) / N - YFORC
            YI(I + 1) = YJ(I)
        END DO
    END DO
    NE = ITER
    I = 0
    !
    !   ECHO INPUT PARAMETERS
    !
    ECODE = -0.003
    BETAI = 0.85 - 0.008 * (30.0 - FPC)
    IF(FPC>=-30.0) BETAI = 0.85
    IF(BETAI<0.65) BETAI = 0.65
    !
    WRITE(6 ,6001) TITLE
    WRITE(6 ,6002) FPC ,FPT ,YOUNGC ,ECO ,ESPALL ,ECTEN
    WRITE(6 ,6003) SK ,SRATIO ,FPCC ,ECC ,ECU
    WRITE(6 ,6004) FY(1) ,FY(2) ,FSU(1) ,FSU(2) ,YOUNGS(1) ,YOUNGS(2) ,YTAN(1) ,1 ,YTAN2
    !
    EYLDC = FY(1) / YOUNGS(1)
    EYLDT = FY(2) / YOUNGS(2)
    WRITE(6 ,6005) EYLDC ,EYLDT ,ESH(1) ,ESH(2) ,ESU(1) ,ESU(2)
    IF(NSEQ==0) GO TO 521
    !
    !   ELEMENTS FOR RECTANGUAR AND GENERAL SECTION
    !   ...........................................
    !
    K = 1
    DO I = 1 ,NE
        511 IF((YJ(I) + YFORC - YJSEX(K))<=0.0.AND.(YI(I) + YFORC - YISEC(K))>=0.0) GO TO 512
        K = K + 1
        IF(K<=NSEC) GO TO 511
        512 AC(I) = (YJ(I) - YI(I)) * XSEC(K)
        DO J = 1 ,NSTEEL
            IF (YS(J)<=YJ(I).AND.YS(J)>YI(I)) AC(I) = AC(I) - AS(J)
        END DO
        ACOVER(I) = (YJ(I) - YI(I)) * XSCOV(K)
        ACORE(I) = AC(I) - ACOVER(I)
        WICONC(I) = WISEC(K)
        WJCONC(I) = WJSEC(K)

    END DO
    GO TO 560
    !
    !   ELEMENTS FOR CIRCULAR SECTION
    !   .............................
    !
    521 PI = 3.1415926536
    THICK = (DIAM - 2.0 * COVER) / 40.
    NCOVER = 0.99 + COVER / THICK
    NE = 2 * NCOVER + 40
    COVERN = NCOVER
    CTHICK = COVER / COVERN
    !
    !   ELEMENT COORDINATE AND AREAS
    !
    RADCOR = 0.5 * DIAM - COVER
    RADCOV = 0.5 * DIAM
    YI(I) = -RADCOV
    IF(NCOVER<1) GO TO 526
    DO N = 1 ,NCOVER
        YJ(N) = YI(N) + CTHICK
        YI(N + 1) = YJ(N)
        ANGLEI = ARSIN(YI(N) / RADCOV)
        ANGLEJ = ARSIN(YJ(N) / RADCOV)
        AREAI = (SIN(ANGLEI) * COS(ANGLEI) + ANGLEI) * RADCOV**2
        AREAJ = (SIN(ANGLEJ) * COS(ANGLEJ) + ANGLEJ) * RADCOV**2
        AC(N) = ABS(AREAJ - AREAI)
        ACOVER(N) = AC(N)
    END DO
    YI(NE - NCOVER + 1) = RADCOR
    DO N = (NE - NCOVER + 1) ,NE
        YJ(N) = YI(N) + CTHICK
        YI(N + 1) = YJ(N)
        ANGLEI = ARSIN(YI(N) / RADCOV)
        ANGLEJ = ARSIN(YJ(N) / RADCOV)
        AREAI = (SIN(ANGLEI) * COS(ANGLEI) + ANGLEI) * RADCOV**2
        AREAJ = (SIN(ANGLEJ) * COS(ANGLEJ) + ANGLEJ) * RADCOV**2
        AC(N) = ABS(AREAJ - AREAI)
        ACOVER(N) = AC(N)
    END DO
    526 YI(NCOVER + 1) = -RADCOR
    DO N = (NCOVER + 1) ,(NE - NCOVER)
        YJ(N) = YI(N) + THICK
        YJ(NE - NCOVER) = RADCOR
        YI(N + 1) = YJ(N)
        ANGLEI = ARSIN(YI(N) / RADCOV)
        ANGLEJ = ARSIN(YJ(N) / RADCOV)
        AREA1 = (SIN(ANGLE1) * COS(ANGLE1) + ANGLE1) * ADCOV**2
        AREA2 = (SIN(ANGLE2) * COS(ANGLE2) + ANGLE2) * ADCOV**2
        ACORE(N) = ABS(AREA2 - AREA1)
        ACOVER(N) = AC(N) - ACORE(N)
        WICONC(N) = 1.0
        WJCONC(N) = 1.0
    END DO
    !
    !   STEEL AREAS AND COORDINATES
    !
    NSTEEL = (NBARS + 1) / 2
    AST = 0.7854 * NBARS * DBARS**2
    FNBARS = NBARS
    THETA = 2.0 * PI / FNBARS
    ANGLE = -0.5 * PI + 0.5 * THETA
    RADST = 0.5 * DIAMST
    ASUM = 0.0
    do N = 1 ,(NSTEEL - 1)
        YS(N) = RADST * SIN(ANGLE)
        ANGLE = ANGLE + THETA
        AS(N) = 0.5 * PI * DBARS**2
        WIS(N) = 1.00
        WJS(N) = 1.00
        ASUM = AS(N) + ASUM
    end do
    YS(NSTEEL) = RADST * SIN(ANGLE)
    AS(NSTEEL) = AST - ASUM
    AS(NSTEEL) = 1.0
    WJS(NSTEEL) = 1.0
    !
    !   CORRECTED CORE AREAS
    !
    DO  I = 1 ,NE
        DO J = 1 ,NSTEEL
            IF(YS(J)<=YJ(I).AND.YS(J)>YI(I)) THEN
                AC(I) = AC(I) - AS(J)
                ACORE(I) = ACORE(I) - AS(J)
            END IF
        END DO
    END DO
    !
    !   ELASTIC STIFFFNESS COFFICIENTS EI EZ EA
    !   =======================================
    !
    560 CONTINUE
    DO N = 1 ,NE
        YSTRIP = YOUNGC * AC(N)
        YHIGH = (YI(N) + YI(J)) * 0.5
        YC(N) = YHIGH
        SECONI = SECONI + 0.017 * AC(N) * WICONC(N) * (SQRT(ABS(FPC)))
        SECONJ = SECPNJ + 0.017 * AC(N) * WJCONC(N) * (SQRT(ABS(FPC)))
        SEI = SECONI + SEHOOP
        SEJ = SECONJ + SEHOOP
        EA = EA + YSTRIP
        EZ = EZ + YSTRIP * YHIGH
        EI = EI + YSTRIP * YHIGH * YHIGH + YSTRIP / 12. * (YJ(N) - YI(N))**2
    END DO
    EIGROS = EI
    NS = NSTEEL
    DO J = 1 ,NS
        YSTRIP = YOUNGS(2) * AS(J)
        EA = EA + YSTRIP
        EZ = EZ + YSTRIP * YS(J)
        EI = EI + YSTRIP * YS(J) * YS(J)
    END DO
    YEX = EZ / EA - YB
    YB = EZ / EA
    EI = EI - EA * (YEX + YB)**2
    FPCAG = FPC * AGROSS
    !
    !   TABULATION OF INPUT DATA
    !   ========================
    !
    WRITE(6 ,6006) TITLE
    ACT = 0.
    ACOV = 0.
    ACOR = 0.
    DO I = 1 ,NE
        ACT = ACT + AC(I)
        ACOV = ACOV + ACOVER(I)
        ACOR = ACOR + ACORE(I)
        WRITE(6 ,6007) I ,YI(I) ,YJ(I) ,YC(I) ,AC(I) ,ACOVER(I) ,ACORE(I)&
                ,WICONC(I) ,WJCONC(I)
        DO J = 1 ,NSTEEL
            IF(YS(J)>YI(I).AND.YS(J)<=YJ(I))
                WRITE(6 ,6008) AS ,WIS ,WJS(J) ,YS(J)
            END IF
        END DO
    END DO

    WRITE(6 ,6009) AGROSS ,ACT ,ACOV ,ACOR ,AST
    RHOS = AST / AGROSS
    RHOCOR = AST / ACOR
    WRITE(6 ,6010) RHOS ,RHOCOR ,FPCAG ,EIGROS ,EI ,EA ,YB
    WRITE(6 ,6111) SEHOOP ,SEHOOP ,SECONI ,SECONJ ,SEI ,SEJ
    !
    !   SHEAR STIFFNESS COEFFICIENTS
    !   ============================
    !
    CONSHR = 4.0 / YOUNGC / SHACON
    SVELAS = 0.4 * YOUNGC * SHACON / FORMF
    SVCRAK = 1.0 / (0.000005 / SHSTNH + CONSHR)
    SVPLAS = 1.0 / (0.000005 / SHSTPL + CONSHR)
    WRITE(6 ,6119) HEIGHT ,HINGEL ,SHACON ,SVELAS ,FORMF ,SHSTNH ,SVCRAK &
            ,SHSTPL ,SVPLAS
    !
    !   INITIALISE
    !   ==========
    !
    DO N = 1 ,NE
        FPEAK(1 ,N) = FPC
        FPEAK(2 ,N) = FPCC
        EPEAK(1 ,N) = ECO
        EPEAK(2 ,N) = ECC
        FCTEN(1 ,N) = FPT
        FCTEN(2 ,N) = FPT
        ECULT(1 ,N) = ESPALL
        ECULT(2 ,N) = ECU
        YC(I) = (YI(I) + YJ(I)) / 2
        IF((2.0 * EPEAK(1 ,N))<=ECULT(1 ,N)) ECULT(1 ,N) = 2.01 * EPEAK(1 ,N)
    END DO
    PFAIL = PF * 1000.
    PPPF = PFAIL / FPCAG
    WRITE(6 ,6011) FP ,PPPF
    !
    !    //////////////////////////////////////////
    !    ROUTINE TO GENERATE AN INTERACTION DIAGRAM
    !    //////////////////////////////////////////
    !
    PHI = 0.0
    WRITE(6 ,631) TITLE
    !
    !       COMPRESSION CAPACITY
    !
    EQ = ECODE
    CALL PMPHI(1 ,5 ,ECODE ,0.0 ,0. ,0. ,0. ,0. ,&
            0 ,0 ,0 ,YI ,YJ ,YC ,ACOVER ,ACOR ,AS ,YS ,&
            NE ,NS ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,&
            BETAI ,TOLERN ,TOLERM ,TOLPHI ,TOLREQ ,&
            EI ,EA ,EZ ,S11 ,S12 ,S21 ,S22 ,PN ,TM ,YB ,PHI ,EQ ,CC)
    PPPUC = PN / FPCAG
    CUC = CC
    TUC = TM
    PUC = PN
    PKN = PUC / 1000.
    TKNM = TUC / 1000000.
    WRITE(6 ,645) PPPUC ,PKN ,TKNM ,TKNM ,PHIUC ,PHIUC ,CUC ,CUC ,ECODE
    WRITE(6 ,632)
    !
    !  TENSION CAPACITY
    !
    EQ = FY(2) / YOUNGS(2)
    CALL PMPHI(2 ,5 ,EQ ,0.0 ,0. ,0. ,0. ,0. ,&
            0 ,0 ,0 ,YI ,YJ ,YC ,ACOVER ,ACORE ,AS ,YS ,&
            NE ,NS ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,&
            BETAI ,TOLERN ,TOLERM ,TOLPHI ,TOLREQ ,&
            EI ,EA ,EZ ,S11 ,S12 ,S21 ,S22 ,PN ,TM ,YB ,PHI ,EQ ,CC)
    PPPT = PN / FPCAG
    PPMIN = PPPT + 0.11
    IF(PPMIN>0.0) PPMIN = -0.001
    CUT = CC
    TUT = TM
    PYT = PN
    PUT = PN
    EYT = EQ
    !
    ! SIMULTANEUS TENSION AND COMPRESSION YIELD
    !
    YSIDE = YS(1)
    ESIDE = FY(1) / YOUNGS(1)
    PHI = (FY(2) / YOUNGS(2) - ESIDE) / (YS(NSTEEL) - YSIDE)
    EQ = ESIDE - PHI * YSIDE
    EY = EQ + YI(1) * PHI
    IF (EY<ECO)
        YSIDE = YI(1)
        ESIDE = ECO
    END IF
    PHIYB = (FY(2) / YOUNGS(2) - ESIDE) / (YS(NSTEEL) - YSIDE)
    PHI = PHIYB
    CALL PMPHI(2 ,5 ,ESIDE ,YSIDE ,0. ,0. ,0. ,0. ,&
            0 ,0 ,0 ,YI ,YJ ,YC ,ACOVER ,ACORE ,AS ,YS ,&
            NE ,NS ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,&
            BETAI ,TOLERN ,TOLERM ,TOLPHI ,TOLREQ ,&
            EI ,EA ,EZ ,S11 ,S12 ,S21 ,S22 ,PN ,TM ,YB ,PHI ,EC ,CC)
    PPPYB = PN / FPCAG
    PYB = PN
    EYB = EQ + YI(1) * PHI
    !
    ! BALANCED ULTIMATE
    !
    PHIUB = (FY(2) / YOUNGS(2) - ECODE) / (YS(NSTEEL) - YI(1))
    PHI = PHIUB
    YSIDE = YI(I)
    CALL PMPHI(1 ,5 ,ECODE ,YSIDE ,0. ,0. ,0. ,0. ,&
            0 ,0 ,0 ,YI ,YJ ,YC ,ACOVER ,ACORE ,AS ,YS ,&
            NE ,NS ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,&
            BETAI ,TOLERN ,TOLERM ,TOLPHI ,TOLREQ ,&
            EI ,EA ,EZ ,S11 ,S12 ,S21 ,S22 ,PN ,TM ,YB ,PHI ,EQ ,CC)
    PPUB = PN / FPCAG
    PUB = PN
    TUB = TM
    !
    ! TOLERANCES
    !
    TOLERN = ABS(FPCAG * 0.0005)
    TOLERM = ABS(TUB * 0.0005)
    XM(1) = TUC / TUB
    YP(1) = PPPUC

    J = 1
    100 J = J + 1
    YP(J) = 0.9 - 0.1 * J
    IF(YP(J)>PPMIN) GO TO 100
    YP(J) = PPPT
    JMIN = J
    JTOTAL = J + 2
    102 J = 1
    103 J = J + 1
    YTEMP = PPPT
    IF(PPPUB>YP(J).AND.PPPUB<YP(J - 1)) YTEMP = PPPUB
    IF(YTEMP==PPPUB) GO TO 105
    IF(PPPYB>YP(J).AND.PPPYB<YP(J - 1)) YTEMP = PPPYB
    IF(YTEMP==PPPYB) GO TO 105
    IF(PPPF>YP(J).AND.PPPF<YP(J - 1)) YTEMP = PPPF
    IF(YTEMP==PPPF) GO TO 105
    IF(J==(JTOTAL + 1)) GO TO 120
    GO TO 103
    105 JREST = JMIN - J
    DO K = 0 ,JREST
        YP(JMIN - K + 1) = YP(JMIN - K)
    END DO
    YP(J) = YTEMP
    JMIN = JMIN + 1
    GO TO 102
    120 XM(JMIN) = TUT / TUB
    NUM = 0
    !
    !   ITERATIVE SOLUTION TO OBTAIN AXIAL LOAD:PNAIM
    !   =============================================
    DO LOAD = 2 ,(JMIN - 1)
        PNAIM = YP(LOAD) * FPCAG
        !
        !      YIELD
        !
        ESIDE = YS(NSTEEL)
        ESIDE = FY(2) / YOUNGS(2)
        IF(PNAIM<PYB) THEN
            YSIDE = YS(1)
            ESIDE = FY(1) / YOUNGS(1)
            IF(EYB<=ECO) THEN
                ESIDE = ECO
                YSIDE = YI(1)
            END IF
        END IF
        5  CONTINUE
        CALL PMPHI(2 ,3 ,ESIDE ,YSIDE ,PNAIM ,0. ,0. ,0. ,&
                NOTDYN ,NUM ,ITER ,YI ,YJ ,YC ,ACOVER ,ACORE ,AS ,YS ,&
                NE ,NS ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,&
                BETAI ,TOLERN ,TOLERM ,TOLPHI ,TOLREQ ,&
                EI ,EA ,EZ ,S11 ,S12 ,S21 ,S22 ,PN ,TM ,YB ,PHI ,EQ ,CC)
        EY = EQ + YI(1) * PHI
        YSIDE = YI(1)
        IF(EC(1)<=ECU) ESIDE = ECU
        IF(EC(1)<ECU) GO TO 5
        TY = TM / 1000000.
        PY = PN / 1000.
        PHIY = PHI
        CY = CC
        YEQ = EQ
        YS11 = S11
        YS12 = S12
        YS21 = S21
        YS22 = S22
        !
        !   ULTIMATE
        !
        ESIDE = ECODE
        YSIDE = YI(1)
        CALL PMPHI(1 ,3 ,ECODE ,YSIDE ,PNAIM ,0. ,0. ,0. ,&
                0 ,0 ,0 ,YI ,YJ ,YC ,ACOVER ,ACORE ,AS ,YS ,&
                NE ,NS ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,&
                BETAI ,TOLERN ,TOLERM ,TOLPHI ,TOLREQ ,&
                EI ,EA ,EZ ,S11 ,S12 ,S21 ,S22 ,PN ,TM ,YB ,PHI ,EQ ,CC)
        TU = TM / 1000000.
        PU = PN / 1000.
        PHIU = PHI
        CU = CC
        !
        !   OUTPUT
        !
        WRITE(6 ,645) YP(LOAD) ,PY ,TU ,TY ,PHIU ,PHIY ,CU ,CY ,EI
        33  IF(PPPUB/=YP(LOAD)) GO TO 34
        WRITE(6 ,663)
        34  IF(PPPYB/=YP(LOAD)) GO TO 35
        WRITE(6 ,634)
        35  IF(PPPF/=YP(LOAD)) GO TO 36
        PHIYF = PHIY * TU / TY
        TYF = TY * 1000000.
        TUF = TU * 1000000.
        TOLERM = ABS(0.005 * TUF)
        WRITE(6 ,635) PHIYF
        36  XM(LOAD) = TU * 1000000. / TUB
        PHI = PHIY
        PN = PY * 1000.
        EQ = YEQ
        S11 = YS11
        S12 = YS12
        S21 = YS21
        S22 = YS22
    END DO
    TU = TUT / 100000.
    PU = PUT / 1000.
    WRITE(6 ,645) PPPT ,PU ,TU ,TU ,PHIUT ,PHIUT ,CUT ,CUT ,EYT
    WRITE(6 ,644)
    !
    !   CRACKING MOMENT
    !   ===============
    !
    PNAIM = PFAIL
    TCRACK = FPT * EI / YOUNGC / YJ(NE) - PNAIM * EI / EA / YJ(NE)
    IF(TCRACK>TYF) TCRACK = TYF
    TMAX = TCRACK
    TCRKNM = TCRACK / 1000000.
    WRITE(6 ,646) PF ,TCRKNM
    !
    !   //////////////////////////////////////////////////////////
    !   ROUTINE TO CALCULATE THE YIELD DEFLECTION AND LATERAL LOAD
    !   OF THE COLUMN WITH AXIAL LOAD(PNAIM) AND HEIGHT(HEIGHT)
    !   QUASI-STATIC STRESS STRAIN RELATIONS ARE USED WITH NO REVERSALS
    !   /////////////////////////////////////////////////////////
    JAY = 0
    40 JAY = JAY + 1
    IF (ACORE(JAY)==0.0) GO TO 40
    III = JAY
    JJJ = NE + 1
    41 JJJ = JJJ - 1
    IF(ACORE(JJJ)==0.0) GO TO 41
    !
    !   INITIALISE
    !
    MOM = 1
    NUM = 0
    PN = 0.
    TM = 0.
    PHI = 0.
    EQ = 0.
    PHIONE = PHIYF * TYF / TUF
    IF(PHIONE>PHIYF) PHIONE = PHIYF
    DPHI = PHIONE / 20.
    PHIAIM = -DPHI
    WRITE(6 ,650)
    !
    !   INCREMENTAL CURVATURE ANALYSIS (DPHI)
    !   ==============================
    !
    DO MOM = 1 ,21
        !
        !      MOMENT-CURVATURE ANALYSIS
        !      -------------------------
        PHIAIM = PHIAIM + DPHI
        CALL PMPHI(2 ,2 ,0.0 ,0.0 ,PNAIM ,0. ,PHIAIM ,0. ,&
                0 ,NUM ,ITER ,YI ,YJ ,YC ,ACOVER ,ACORE ,AS ,YS ,&
                NE ,NS ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,0.0 ,&
                0.0 ,TOLERN ,TOLERM ,TOLPHI ,TOLREQ ,&
                EI ,EA ,EZ ,S11 ,S12 ,S21 ,S22 ,PN ,TM ,YB ,PHI ,EQ ,CC)
        44        PHIMU = PHI / PHIYF
        PKN = PN / 1000.
        TKNM = TM / 1000000.
        TRATIO = TM / TUF
        !
        !       DETERMINE EFFECTIVE STIFFNESS
        !       -----------------------------
        !
        TNMM(MOM) = TM
        PHIMM(MOM) = PHI
        DELTMM = 0.
        IF(MOM==1) GO TO 46
        !
        DO N = 2 ,MOM
            FIRST = PHIMM(N - 1) * (TNMM(N) + 2.0 * TNMM(N - 1))
            SECND = PHIMM(N) * (2.0 * TNMM(N) + TNMM(N - 1))
            DENOM = 6.0 * TNMM(MOM)**2
            DELTMM = DELTMM + (FIRST + SECND) * (TNMM(N) - TNMM(N - 1)) / DENOM
        END DO
        46     IF(MOM==1) THEN
            EIEFF = EI
            ERATIO = EIEFF / EIGROS
        ELSE IF(MOM>1) THEN
            ERATIO = DELTGR / DELTMM
        END IF
        DELTGR = TNMM(MOM) / (3.0 * EIGROS)
        EIEDD = EIGROS * ERATIO
        !
        !       DEFLECTIONS
        !       -----------
        !       ELASTIC DEFLECTION = DEFNE
        DEFNE = TM * HEIGHT * HEIGHT / EIEFF / 3.0
        !
        !       APPROXIMATE SHEAR DEFLECTION = DEFNS
        !
        IF(ABS(TM)>TMAX) TMAX = ABS(TM)
        QVELAS = TCRACK / TMAX
        QVPLAS = 1.0 - QVELAS
        QVCRAK = 0.0
        IF(QVPLAS>(HINGEL / HEIGHT))
            QVCRAK = 1.0 - HINGEL / HEIGHT - QVELAS
            QVPLAS = HINGEL / HEIGHT
        END IF
        DEFNS = TM * (QVELAS / SVELAS + QVCRAK / SVCRAK + QVPLAS / SVPLAS)

        !
        !       TOTAL DEFLECTION = DEFNT
        !
        DEFNT = DEFNE + DEFNS
        WRITE(6 ,651) PHIMU ,TRATIO ,PHI ,TKNM ,CC ,FS(1) ,FS(NSTEEL) ,EC(1)&
                ,FCONC(1 ,1) ,EC(JAY) ,FCONC(2 ,JAY) ,EIEFF ,ERATIO ,DEFNT ,PKN ,ITER
    END DO

    !
    !   YIELD DEFLECTION AT IDEAL STRENGTH
    !
    DEFNE = DEFNE * TUF / TM
    DEFNS = DEFNS * TUF / TM
    DEFNY = DEFNE + DEFNS
    YFORCE = TUF / HEIGHT / 1000.
    WRITE(6 ,652) DEFNE ,DEFNS ,DEFNY ,YFORCE
    !
    !   ///////////////////////////////////////////////////////
    !   LATERAL LOAD-DEFLECTION AND MOMET-CURVATURE ANALYSIS
    !   CYCLIC LOADING AND DYNAMIC MATERIAL EFFECTS CONSIDERED
    !   //////////////////////////////////////////////////////
    !
    TMAX = TCRACK
    !
    !   INITIALISE
    !
    MOM = 1
    NUM = 0
    PN = 0.
    TM = 0.
    EQ = 0.
    SFORCE = 0.0
    !
    !   READ CARD FOR LOADING DIRECTION
    !
    CALL READR(2 ,1 ,0 ,RRDR ,IRDR)
    WRITE(6 ,660) INPUT
    IF(RRDR(1)==0.0) GO TO 1000
    IPRINT = IRDR(1)
    SIGN = (ABS(RRDR(1))) / RRDR(1)
    DPHI = 0.1 * SIGN * PHIYF
    PHIAIM = -DPHI
    PHI = -DPHI
    IF((ABS(RRDR(2)))>=0.01) NOTDYN = 1
    IF(NOTDYN==1) TIMER = -0.1 * (ABS(RRDR(2)))
    !
    !   MOMENT CURVATURE ANALYSIS
    !   =========================
    !
    200 PHIAIM = PHIAIM + DPHI
    IF(NOTDYN==1) THEN
        PHRATE = ABS(PHIYF / RRDR(2))
        TIME = ABS(DPHI / PHRATE)
    END IF
    TIMER = TIMER + TIME
    PNAIM = PFAIL + VRATIO * SFORCE * 1000.
    IF((ABS(PHIMU))>1.1) TOLERN = ABS(FPCAG * 0.005)
    CALL PMPHI(3 ,2 ,0. ,0. ,PNAIM ,0. ,PHIAIM ,0. ,&
            NOTDYN ,NUM ,ITER ,YI ,YJ ,YC ,ACOVER ,ACORE ,AS ,YS ,&
            NE ,NS ,EDOTF ,EDOTY ,DF ,DY ,DINDEX ,TIME ,&
            0.0 ,TOLERN ,TOLERM ,TOLPHI ,TOLREQ ,&
            EI ,EA ,EZ ,S11 ,S12 ,S21 ,S22 ,PN ,TM ,YB ,PHI ,EQ ,CC)
    !
    ! UPDATE STRAIN HISTORY FOR CONCRETE
    !
    DO N = 1 ,NE
        WCSEP = 0.0
        IF(EC(N)<ECIN(N).AND.FCONC(2 ,N)<0.00) THEN
            WCSTEP = ABS((EC(N) - ECMIN(N)) * FCONC(2 ,N))
        END IF
        WDI = WDI + WCSTEP * ACORE(N) * WICONC(N)
        WDJ = WDJ + WCSTEP * ACORE(N) * WJCONC(N)
        ECOLD(N) = ECLAST(N)
        ECLAST(N) = EC(N)
        FCLAST(1 ,N) = FCONC(1 ,N)
        FCLAST(2 ,N) = FCONC(2 ,N)
    END DO
    !
    ! UPDATE STRAIN HISTORY FOR STEEL
    !
    DO N = 1 ,STEEL
        WSSTEEL = 0.0
        IF(ELAST(K)<=EMAX(1 ,N).AND.FS(N)<0.) THEN
            WSSTEP = FS(N) * (E(N) - ELAST(N))
        END IF
        WDI = WDI + WSSTEP * AS(N) * WIS(N)
        WDJ = WDJ + WSSTEP * AS(N) * WJS(N)
        EOLD(N) = ELAST(N)
        ELAST(N) = E(N)
        FLAST(N) = FS(N)
    END DO
    !
    ! RESULTS
    !
    PHIMU = PHI / PHIYF
    PKN = PN / 1000.
    TKNM = TM / 1000000.
    TRATIO = TM / TUF
    IF(ITER>=10) GO TO 220
    !
    ! DEFLECTION CALCULATION FOR MOMENT CURVATURE
    !
    IF((ABS(TM))>TMAX) TMAX = ABS(TM)
    !
    ! ELASTIC DEFLECTION
    !
    DEFNE = TM * HEIGHT * HEIGHT / EIEFF / 3.0
    !
    ! PLASTIC DEFLECTION
    !
    IF(TMAX>TYF) HPLAST = HEIGHT * (1.0 - TYF / TMAX)
    ROTATN = (PHI - PHIONE * TM / TYF) * (HPLAST / 3. + YPTRAT)
    DEFNP = ROTATN * (HEIGHT - HPLAST / 4.0)
    !
    ! SHEAR DEFLECTION
    !
    QVELAS = TCRACK / TMAX
    QVPLAS = 1.0 - QVELAS
    QVCRAK = 0.0
    IF(QVPLAS>(HINGEL / HEIGHT)) THEN
        QVCRAK = 1.0 - HINGEL / HEIGHT - QVLAS
        QVPLAS = HINGEL / HEIGHT
    END IF
    DEFNS = TM * (QVELAS / SVELAS + QVCRAK / SVCRAK + QVPLAS / SVPLAS)
    !
    ! TOTAL DEFLECTION, SHEAR FORCE AND DUCTIBILITY
    !
    ODEFNT = DEFNT
    DEFNT = DEFNE + DEFNP + DEFNS
    DUCTMU = DEFNT / DEFNY
    SFORCE = (TM + PN * DEFNT * PDELTA) / HEIGHT / 1000.
    !
    ! OUTPUT RESULTS
    !
    YMOM(MOM) = TRATIO
    XPHI(MOM) = PHIMU
    XDFN(MOM) = DEFNT
    YFOR(MOM) = SFORCE / YFORCE
    WERKI = WDI / SEI
    WERKJ = WDJ / SEJ
    IF(IPRINT==0) go to 220
    WRITE(6 ,661) TIMER ,DEFNE ,ITER
    WRITE(6 ,662) TKNM ,TRATIO(E1) ,FS(1) ,E(NSTEEL) ,FS(NSTEEL) ,DEFNP ,KPN
    WRITE(6 ,663) PHI ,PHIMU ,EC(III) ,FCONC(2 ,III) ,EC(JJJ) ,FCONC(2 ,JJJ) ,&
            DEFNS ,SFORCE
    WRITE(6 ,664) EQ ,WERKI ,WERKJ ,EC(1) ,FCONC(1 ,1) ,EC(NE) ,FCONC(1 ,NE)&
            ,DEFNT ,DUCTMU
    !
    ! CHECK LIMITS
    !
    IF(ITER>=10) THEN
        MOM = MOM - 1
        WRITE(6 ,668) PHIMU
    END IF
    IF(EC(III)<ECU.OR.EC(JJJ)<ECU) GO TO 1000
    IF(((DEFNT - DEFNL) * SIGN)>=0.0) THEN
        NUM = 0
        DEFNO = DEFNL
        CALL READR(2 ,1 ,0 ,RRDR ,IRDR)
    END IF
    IPPRINT = IRDR(1)
    IF(NUM==0) WRITE(6 ,670) IPRINT ,INPUT
    IF(RRDR(1)==0.0) GO TO 1000
    DEFNL = RRDR(1)
    SIGN = (ABS(DEFNL - DEFNO)) / (DEFNL - DEFNO)
    IF((ABS(RRDR(2)))>=0.01) NOTDYN = 1
    DPHINU = 1.1 * DPHI * (DEFNL - DEFNT) / (DEFNT - ODEFNT)
    DPHI = 0.2 * SIGN * PHIYF
    IF(NUM>10) DPHI = 0.5 * SIGN * PHIYF
    IF(NUM>20) DPHI = 1.0 * SIGN * PHIYF
    IF(NUM>2.AND.(ABS(DPHI))>(ABS(DPHINU))) DPHI = DPHINU
    220 IF (ITER>=10) MOM = MOM + 1
    GO TO 200
    1000 CONTINUE
    CALL AGRAPH(XPHI ,YMOM ,MOM)
    CALL AGRAPH(XDFN ,YFOR ,MOM)
    STOP
    ! FORMATS FOR THE PROGRAM
    !
    501 FORMAT(80A1)
    600 FORMAT(1H0 ,132(1H)//51X ,23HCOLUMN ANALYSYS PROGRAM/1H0 ,132(1H)/)
    601 FORMAT(7H 1 JOB ,34X ,3H ,80A1 ,3H)
    602 FORMAT(24H  2 CONCRETE UNCONFINED ,17X ,3H ,80A1 ,3H)
    603 FORMAT(24H  3            CONFINED ,17X ,3H22740 ,80A1 ,3H)
    604 FORMAT(22H0 4 STEEL COMPRESSION ,19X ,3H ,80A1 ,1H)
    605 FORMAT(22H  5       TENSION ,19X ,3H ,80A1 ,1H)
    606 FORMAT(22H0 6 CONTROL PARAMETERS ,19X ,3H ,80A1 ,3H)
    6071 FORMAT(31H  7 CIRCULAR SECTION PROPERTIES ,10X ,3H ,80A1 ,3H)
    6073 FORMAT(I3 ,17H CONCRETE SECTION ,21X ,3H ,80A1 ,3H)
    608 FORMAT(I3 ,16H REBAR POSITIONS ,22X ,3H ,80A1 ,3H)
    609 FORMAT(1H0 ,I2 ,17H SHEAR PROPERTIES ,21X ,3H ,80A1 ,3H/)
    610 FORMAT(1H0 ,I2 ,11H AXIAL LOAD ,27X ,3H ,80A1 ,3H/)
    6001 FORMAT(1H1 ,132(1H)//11X ,24HMATERIAL PROPERTIES FOR ,80A1 ,/1H0 ,132(1H)//)
    6002 FORMAT(31H0UNCONFINED CONCRETE PARAMETERS // &
            31X ,35H'COMPRESSION STRENGTH                      =' ,F8.1// &
            31X ,35H'TENSION STRENGRH................. =' ,F9 ,2// &
            31X ,35H'YOUNGS MODULUS................... =' ,F7.0// &
            31X ,35H'SPALLING STRAIN.................. =' ,F11.4// &
            31X ,35H'TENSILE STRAIN................... =' ,F11.4//)
    6003 FORMAT(31H0  CONFINED CONCRETE PARAMETERS// &
            31X ,35H 'STRENGTH RATIO............... =' ,F9.2// &
            31X ,35H 'STRAIN/STRENGTH RATIO........ =' ,F10.3// &
            31X ,35H 'CONFINED STRENGTH.............=' ,F8.1// &
            31X ,35H 'CONFINED PEAK STRAIN......... =' ,F11.4// &
            31X ,35H 'ULTIMATE STRAIN =' ,F11.4)
    6004 FORMAT(19H0LONGITUDINAL STEEL ,47X ,20HCOMPRESSION TENSION //&
            31X ,35H'YIELD STRESS.............. , =' ,F8.1 ,F13.1//&
            31X ,35H'ULTIMATE STRESS...........  =' ,F8.1 ,F13.1//&
            31X ,35H'YOUNGS MODULUS............  =' ,F7.0 ,F13.0//&
            31X ,35H'STRAIN-HARDENING MODULUS..  =' ,F7.0 ,F13.0/)
    6005 FORMAT(31X ,37H'YIELD STRAIN............... =' ,F9.4 ,F13.4//&
            31X ,37H'STRAIN-HARDENING........... =' ,F9.4 ,F13.4//&
            31X ,37H0'ULTIMATE STRAIN............ =' ,F9.4 ,F13.4)
    6006 FORMAT(17HSECTION ELEMENTS ,9X ,80A1 ,/26X ,80(1H)////&
            /58H STRIP     YI        YJ        YC     ACONC    ACOVER ,&
            60H  ACORE (WIC) (WJC)         ASTELL      (WIS) (WJS) ,&
            8H      YS/)
    6007 FORMAT(I5 ,6F10.1 ,2H (F5.3 ,3H) (F5.3 ,1H) ,F10.1)
    6009 FORMAT(1H ,132(1H)/25H AREA TOTALS: ('GROSS=' ,F10.1 ,1H) ,&
            F9.1 ,2F10.1 ,25X ,F10.1///)
    6008 FORMAT(1H'+' ,89X ,F10.1 ,3H (F5.3 ,3H)  (F5.3 ,1H) ,F10.1)
    6010 FORMAT(1H1 ,30X ,32H'VOLUMETRIC STEEL CONTENT-GROSS =' ,F7.4//&
            55X ,8H'-CORE.... =' ,F7.4//&
            31X ,11H'FPC*AG... =' ,1P ,E11.3//&
            31X ,11H'EI-GROSS.. =' ,1P ,E11.3//&
            31X ,11H'EI........ =' ,1P ,E11.3//&
            31X ,11H'EA........=' ,1P ,E11.3//&
            631X ,11H'Y-CENTROID =' ,1P ,E11.3//)
    6111 FORMAT(14X ,50HSTRAIN ENERGY CAPACITY ('UNITS=J/M')  I J//&
            31X ,13HHOOPS (GIVEN) ,1P ,2E11.3//&
            31X ,13HCORE CONCRETE ,1P ,2E11.3//&
            31X ,13HTOTAL ENERGY ,1P ,2E11.3//)
    6119 FORMAT(17H COLUMN HEIGHT ,F10.0 ,3H MM//&
            16H CONFINED LENGTH ,F10.0 ,3H MM//&
            18H ('FORM FACTOR=' ,F5.2 ,1H)//&
            29H 'SHEAR STEEL (AV*D/S)' ,F11.0 ,16H 'MM2  STIFFNESS =' ,E11.4 ,&
            29H 'HINGE SHEAR STEEL (AV*D/S)' ,F11.0 ,16H 'MM2 STIFFNESS = ' ,E11.4/)
    6011 FORMAT(21H0  'COLUMN AXIAL LOAD=' ,F11.2 ,23H KN ,'AXIAL LOAD RATIO =' ,&
            F8.6 ,7H 'FPC*AG'//)
    631  FORMAT(35H1'MOMENT -AXIAL LOAD INTERACTION FOR' ,80A1// &
            35X ,80(1H)/// &
            24X ,6H'FPC*AG' ,9X1HP ,12X ,2HMU ,10X ,2HMY ,8X ,2HPHIU ,10X ,4HPHIY ,&
            47X ,2HCU ,7X ,2HCY ,10X ,2HEY/)
    632 FORMAT(21H'+COMPRESSION CAPACITY')
    633 FORMAT(18H'+BALANCED ULTIMATES')
    634 FORMAT(19H'+OUTER STEEL YIELDS')
    635 FORMAT(11H'+STUDY CASE'/9H  'PHIYF =' ,1P ,E12.5 ,111(1H))
    644 FORMAT(17H'+TENSION CAPACITY')
    645 FORMAT(1H0 ,23X ,F6.3 ,3F12.0 ,2(3X ,1P ,E11.3) ,2(1X ,0P ,F8.1) ,F12.6)
    646 FORMAT(1H0 ,132(1H)// &
            35H 'CRACKING MOMENT (FOR AXIAL LOAD OF' ,F6.0 ,6H 'KN) = ' ,F8.1 ,4H KNM/)
    650 FORMAT(1H0 ,132(1H) ,//&
            51X ,51H'YIELD DEFLECTION FROM QUASI-STATIC MOMENT CURVATURE' ,/&
            20H0 PHI M ,27X ,33H REINFORCING COVER CONCRETE ,&
            52H CORE CONCRETE EIEFF EI DEFN AXIAL N ,&
            /60H '---- - CURVATURE MOMENT C FSC FST' ,&
            47H 'EC FC EC FC 2 ---' ,18X ,7H'LOAD U' ,&
            /61H 'PHIY MI (RAD/MM) (KN-M) (MM) (MPA) (MPA)' ,&
            8X ,8H (MPA) ,10X ,20H ('MPA (N-MM)') EIG (MM) ,9X ,7H(KN) M/)
    651 FORMAT(1H ,F8.3 ,F7.3 ,5X ,1P ,E11.3 ,0P ,F9.1 ,F8.1 ,2F6.0 ,F10.6 ,F6.1 ,&
            F10.6 ,F6.1 ,1P ,E9.2 ,0P ,F6.3 ,F8.2 ,5X ,F9.0 ,1X ,I1)
    652 FORMAT(1H0 ,50X ,18H'YIELD DEFLECTION =' ,F6.2 ,16H 'MM (FLEXURAL) + ' ,&
            F6.2 ,13H 'MM (SHEAR) = ' ,F6.2 ,11H 'MM (TOTAL)' ,&
            //1H ,50X ,18H'YIELD FORCE = ' ,F6.0 ,3H KN//1H ,132(1H))
    660 FORMAT(1H1 ,132(1H)//50X ,23HCYCLIC LOADING ANALYSIS//1H ,132(1H)/&
            36H0 'LIMIT AND RATE FOR FIRST CYCLE ***' ,80A1 ,3H'***')
    661 FORMAT(11H 'TIME=' ,F11.3 ,4H 'SEC' ,31X ,15HI 'STRAIN STRESS' ,&
            32H 'J STRAIN STRESS ELASTIC DEFN =' ,F7.2 ,I10 ,11H 'ITERATIONS')
    662 FORMAT(11H 'MOMENT=' ,F11.3 ,4H SEX ,31X ,15 HI STRAIN STRESS ,&
            32H 'J STRAIN STRESS ELASTIC DEFN =' ,F7.2 ,I10 ,11H 'ITERATIONS')
    663 FORMAT(11H 'CURVATURE=' ,1P ,E114 ,18H RAD/MM ('PHI/PHIY =' ,0P ,F7.3&
            ,8H) CORE ,F10.6 ,F7.1 ,F11.6 ,F7.1 ,14H 'SHEAR DEFN = ' ,F7.2 ,&
            10H 'FORCE =' ,F8.1 ,3H KN)
    664 FORMAT(11H 'STRAIN E0=' ,F11.6 ,8H 'WORK I =' ,F7.3 ,3H 'J =' ,F7.3 ,&
            8H COVER ,F10.6 ,F7.1 ,F11.6 ,F7.1 ,14H 'TOTAL DEFN = ' ,F7.2 ,&
            15H'MM DUCTILITY =' ,F6.2/1H0 ,132(1H)/)
    668 FORMAT(35H0 'DID NOT CONVERGE FOR PHI/PHIY=' ,F7.3/1H0 ,132(1H)/)
    670 FORMAT(I1 ,37H 'LIMIT AND RATE FOR THIS CYCLE ***' ,80A1 ,3H'***/')
end program columns